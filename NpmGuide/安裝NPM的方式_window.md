管理你的javascript透過NPM  
=====
----
### 列出可以安裝的 node version.    
	nvm list available   
 
----
### 安裝特定版本軟體，記得沒有帶入save，表示沒有寫入package.json
	nvm install <version_number>  
例如 :12.14.0(版本號碼)  

----
### 啟用特定的node version  
	nvm use <version_number>   
例如 :12.14.0(版本號碼)  

----
### 顯示node version  
	node -v  

----
### 顯示 npm version  
	npm -v  

----
### 初始化你的javasceript專案(以JQuery為例)  
#### 這個動作是初始化 package.json 所有有關於 專案的 javascript library 管理都寫在這個檔案內。    
	npm init
#### 可以檢查 package.json 大概會是這樣的   
    {
      "name": "temp",
      "version": "1.0.0",
      "description": "",
      "main": "index.js",
      "dependencies": {
        "jquery": "^3.4.1"
      },
      "devDependencies": {},
      "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1"
      },
      "author": "",
      "license": "ISC"
    }
	
#### 這時候再執行指令 
	npm isntall jquery
#### 如下顯示  
    npm WARN temp@1.0.0 No description
    npm WARN temp@1.0.0 No repository field.
    
    + jquery@3.4.1
    updated 1 package and audited 1 package in 0.812s
    found 0 vulnerabilities
#### 但是這時候只是把檔案下載到node_module而已，並未寫入package.json，並未列入管理，必須執行
	npm install jquery --save
#### 才算是完整管理檔案	
	


	

	