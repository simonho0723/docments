Vagrant Guide (base on Vagrant 2.2.6, VirturalBox 6.0.14)
====
## 基本安裝方式   
方便在同一台機器上設定VM，並且可以有相同的配置環境  
window 安裝方式  
請先安裝virtural-box 跟 vagrantvirtualbox 安裝時候，請安裝c:\底下。   
安裝後,執行指令,顯示版本
    
    vagrant --version   


## 參考文件   
https://www.vagrantup.com/docs/   
預設的box 目錄  
使用者\.vagrant.d\box  
EX:  

    C:\Users\user\.vagrant.d\boxes  

可以透過設定 VAGRANT_HOME 環境變數將 box，指到另外一個地方。  

----
## Add New Image   
加入centos 7 影像檔案    
    
    vagrant box add centos/7      
  
## 更新ALL PLUGIN    
    vagrant plugin update

## Update New Image   
更新已經存在的影像檔案   

    vagrant box update --box centos/7    

## List installed images   
列出已經存在的影像檔案   

    vagrant box list    

## Check if have newest images    
檢查是否有新的影像檔案     
 
    vagrant box outdated

## 初始化資料夾   

在自己的專案資料夾,執行初始化VM的動作，產生設定檔案 Vagrantfile(需要在自己的資料夾執行)  
如果已經有存在 Vagrantfile ，則我們可以不用做這個動作  

    vagrant init centos/7

## Install Plugin

安裝 virtualbox plugin，讓資料夾可以共享(記得先行安裝)  
    
    vagrant plugin install vagrant-vbguest

安裝可以控制硬碟大小的參數(記得先行安裝)  

    vagrant plugin install vagrant-disksize

    vagrant plugin install vagrant-winrm

安裝分享你的SSH，透過Http跟SSH(記得先行安裝) ---不必使用，有很多問題。  
https://www.vagrantup.com/docs/cli/share.html  
https://www.vagrantup.com/docs/cli/connect.html  
下載ngrok   
解壓縮之後，將 ngrok.exe 放入 PATH 變數。   

    vagrant plugin install vagrant-share

修改 Vagrantfile
----
[VagrantExamples](/VagrantGuide/Vagrantfile)范例
    

驗證檔案 Vagrantfile
---
    vagrant validate

啟動設定好的vm(需要在自己的資料夾執行)
---
    vagrant up

關閉機器(需要在自己的資料夾執行)
---
    vagrant halt
在初始化結束之後，連進ssh conection(需要在自己的資料夾執行)  
---
    vagrant ssh
刪除設定好的配置(透過init)建立(需要在自己的資料夾執行)   
---
    vagrant destroy
重新載入設定檔案，不做初始化的動作
---
    vagrant reload
修改Vagrantfile之後，要重新載入(每次都要下一次，才會重新生效)，後面帶入provision(表示初始化動作重新執行一次)
---
     vagrant reload --provision

重新執行 啟動後的指令, 預設config.vm.provision只會執行一次, 可以透過下面的指令重複執行(需要在自己的資料夾執行)或是加上run: always   

    
    config.vm.provision "shell", run: "always" ,inline: <<-SHELL
       yum update -y
    SHELL

重新在執行  

    vagrant provision 




關於vm 權限資源控制請參考  
---
https://www.vagrantup.com/docs/virtualbox/configuration.html  
https://www.virtualbox.org/manual/ch08.html#vboxmanage-modifyvm  
https://www.virtualbox.org/manual/ch06.html#network_bandwidth_limit   



解決遠端SSH的方式(手動執行)    
----
You need to ssh to the vm as usual and then edit /etc/ssh/sshd_config.
There you need to set PasswordAuthentication(ChallengeResponseAuthentication ) to yes instead of no. This will allow password authentication.    

    sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
    sudo systemctl restart sshd




錯誤整理:
---
錯誤一:  
中間這段參數常常會因為(Virtualbox或是Vagrant)版本的不同而遇到問題。   
所以在reload的時候，或許不要再重新設定VM參數，因為VM已經建立，有的時候參數已經設定過了。不能重新設定。   

     config.vm.provider "virtualbox" do |vb|
        # Display the VirtualBox GUI when booting the machine
        vb.gui = false
        # set cpu limit.
        vb.customize ["modifyvm", :id, "--cpuexecutioncap", "50"]
        # network limit.
        vb.customize ["bandwidthctl", :id, "add", "Limit", "--type", "network", "--limit",    0m" ]
        # Customize the amount of memory on the VM:
        vb.m#emory = 1024
        vb.cpus = 1
      end 

錯誤二(重新啟動的時候，會有這樣訊息):  

    <Vagrant::Errors::VBoxManageError: There was an error while executing `VBoxManage`, a CLI d by Vagrant
    for controlling VirtualBox. The command and stderr is shown below.
    Command: ["bandwidthctl", "82ec53e2-c036-4b51-bef5-6e02ad902add", "add", "Limit", type", "network", "--limit", "20m"]
    Stderr: VBoxManage.exe: error: Bandwidth group named 'Limit' already exists

主要是因為第一個virtualbox參數，第一次已經建立。第二次不需要再跑，但她每次還是會執行。


錯誤三(重新啟動的時候，會有這樣訊息):   

    Vagrant was unable to mount VirtualBox shared folders. This is usually
    because the filesystem "vboxsf" is not available. This filesystem is
    made available via the VirtualBox Guest Additions and kernel module.
    Please verify that these guest additions are properly installed in the
    guest. This is not a bug in Vagrant and is usually caused by a faulty
    Vagrant box. For context, the command attempted was:
    
    mount -t vboxsf -o uid=1000,gid=1000 vagrant_data /vagrant_data
    
    The error output from the command was:
    
    /sbin/mount.vboxsf: mounting failed with the error: Protocol error

 原因出在多掛了一個軟連結，因為這個資料夾(vagrant_data)也是透過mount上去

    config.vm.provision "shell", run: "always" ,inline: <<-SHELL
     yum update -y
     setenforce 0
     ln -s /vagrant_data/ /home/vagrant/     
    SHELL
 
 另外解決方式:   
 請重新移除virtualbox 再重新安裝，但是移除請刪除使用者下面的，預設資料夾(C:\Users\user\.tualBox)，達到完整清除。    
 然後重新安裝新的plugin，然後電腦關機後重開幾次。    



問題四  
出現下列問題   

     ["startvm", "47af6fe7-aa62-4aa6-89d8-e2518f364183", "--type", "headless"]
     
     VBoxManage.exe: error: Details: code E_FAIL (0x80004005), component ConsoleWrap, interface     console
      
請重新移除virtualbox 再重新安裝，但是移除請刪除使用者下面的，預設資料夾(C:\Users\user\.VirtualBox)，達到完整清除。   





