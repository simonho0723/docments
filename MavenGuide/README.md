# maven 使用手冊

###  參數優化

     set MAVEN_OPTS=-Xmx2048m -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=256m -XX:+TieredCompilation -XX:TieredStopAtLevel=1 -XX:+CMSClassUnloadingEnabled -Dfile.encoding=UTF-8  -Djava.security.egd=file:/dev/./urandom

### 產生 template 方式  

     mvn archetype:generate 

如果

     是 JDK 1.8 請選擇 2665 
     是 JDK 11  請選擇 2664

