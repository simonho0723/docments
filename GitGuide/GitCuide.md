Git Guide Tourtal( base on git 2.24.0 )
========================

基本使用  
---  
設定預設姓名     

    git config --global user.name "Simon Ho"

設定預設email
---
    git config --global user.email "simonho0723@gmail.com"

clone你的專案  
---
    git clone https://gitlab.com/simonho0723/docker-compose.git

clone你的專案(帶入帳密)
---
    git clone https://<username>:<password>@gitlab.com/simon0723/play-e.git 
    git clone https://tony.ct:8DUcgNpm@gitlab.com/simonho0723/play-e.git


新増變更
---
    git add .

commit 你現在的變動  
---  
    git commit -m " add base version"

push 檔案到 remote server.
---
    git push

比對之間版本的變動
---
    git diff
    
檢視log
---
    git log
    
檢查特定commit資料  
---
    git show <commit_id>

問題處理:
---
避免  
warning: LF will be replaced by CRLF in postgres/XXXX.sh. 錯誤產生執行

    git config --global core.autocrlf true  
    git config --global core.safecrlf true  
